<?php
if ( !defined('ABSPATH') ) {
    exit;
}

use Carbon_Fields\Block;
use Carbon_Fields\Field;

if ( !class_exists('ThemeCustomBlocks') ) {

    class ThemeCustomBlocks {

        private static $_instance = null;

        private $globalContainer = null;

        public static function instance()
        {
            if ( is_null(self::$_instance) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        public function __construct()
        {
            $this->bootCarbonFields();
        }

        private function bootCarbonFields()
        {
            add_action('carbon_fields_register_fields', array(&$this, 'shortPersoneInfo'), 10);
            add_action('carbon_fields_register_fields', array(&$this, 'awards'), 20);
            add_action('carbon_fields_register_fields', array(&$this, 'layoutItem'), 30);
            add_action('carbon_fields_register_fields', array(&$this, 'blockquoteWithAuthor'), 40);
            add_action('carbon_fields_register_fields', array(&$this, 'dotsList'), 50);
            add_action('carbon_fields_register_fields', array(&$this, 'sliderGallery'), 60);
            add_action('carbon_fields_register_fields', array(&$this, 'cellGallery'), 70);
            add_action('carbon_fields_register_fields', array(&$this, 'sliderSorting'), 80);
            add_action('carbon_fields_register_fields', array(&$this, 'filesUpload'), 90);
            add_action('carbon_fields_register_fields', array(&$this, 'acordeonBlock'), 100);
            add_action('carbon_fields_register_fields', array(&$this, 'scheduleBlock'), 110);
            add_action('carbon_fields_register_fields', array(&$this, 'tagItem'), 120);
            add_action('carbon_fields_register_fields', array(&$this, 'socialItem'), 130);
            add_action('carbon_fields_register_fields', array(&$this, 'universalAcordeonBlock'), 130);
        }

        public function shortPersoneInfo()
        {
            Block::make( __( 'Short Persone Info' ) )
                ->add_fields(array(
                    Field::make( 'select', 'persone_size', 'Размер' )
                        ->set_default_value( 'user' )
                        ->add_options( array(
                            'user' => 'Средний',
                            'user user--main' => 'Большой',
                        ) ),
                    Field::make('complex', 'persone', 'Persone')
                        ->set_collapsed(true)
                        ->add_fields( array(
                            Field::make( 'text', 'persone_name', __( 'Name' ) ),
                            Field::make( 'image', 'persone_image', __( 'Photo' ) ),
                            Field::make( 'rich_text', 'persone_description', __( 'Description' ) ),
                            Field::make( 'text', 'persone_link', __( 'Person link' ) ),
                        ) )
                        ->set_header_template('
                            <% if (persone_name) { %>
                                <%- persone_name %>
                            <% } else { %>
                                Новая персона
                            <% } %>
                        '),
                ))
                ->set_icon( 'id' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields ) {
                    if(!empty($fields['persone'])){
                        foreach($fields['persone'] as $persone){
                            ?>
                            <a href="<?php if(empty($persone['persone_link'])){?>javascript:void(0)<?php }else{ echo esc_html( $persone['persone_link'] );}?>" class="<?php echo $fields['persone_size'];?>">
                                <div class="user__img">
                                    <img src="<?php if(empty($persone['persone_image'])){ echo get_template_directory_uri()."/assets/app/img/user-ava.png"; }else{ echo wp_get_attachment_image_url( $persone['persone_image']);}?>" alt=""
                                         class="object-fit is--cover" data-object-fit="cover">
                                </div>

                                <div class="user__info">
                                    <div class="user__info-name">
                                        <p><?php echo esc_html( $persone['persone_name'] ); ?></p>
                                    </div>

                                    <div class="user__info-text">
                                        <?php echo apply_filters( 'the_content', $persone['persone_description'] ); ?>
                                    </div>
                                </div>
                            </a>
                            <?php
                        }
                    }
                } );
        }

        public function awards()
        {
            Block::make( 'awards', __( 'Награды' ) )
                ->add_fields(array(
                    Field::make('complex', 'awards_loop', 'Persone')
                        ->set_collapsed(true)
                        ->add_fields( array(
                            Field::make( 'text', 'award_name', __( 'Название' ) ),
                            Field::make( 'text', 'award_description', __( 'Описание' ) ),
                        ) )
                        ->set_header_template('
                            <% if (award_name) { %>
                                <%- award_name %>
                            <% } else { %>
                                Новая награда
                            <% } %>
                        '),
                ))
                ->set_icon( 'awards' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields ) {
                    if(!empty($fields['awards_loop'])){
                        foreach($fields['awards_loop'] as $award){
                            ?>
                            <div class="user__progress-contains">
                                <svg class="user__progress-icon medal-icon">
                                    <use class="medal-icon__part"
                                         xlink:href="<?php echo get_template_directory_uri()?>/assets/app/img/icons/sprite.svg#medal">
                                </svg>

                                <div class="user__progress-info">
                                    <p><strong><?php echo $award['award_name'];?></strong></p>
                                    <?php if(!empty($award['award_description'])){?>
                                        <p><?php echo $award['award_description'];?></p>
                                    <?php }?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                } );
        }

        public function layoutItem()
        {
            /*if(is_admin()){
                wp_register_style(
                    'crb-my-shiny-gutenberg-block-stylesheet',
                    get_stylesheet_directory_uri() . '/carbon-styles.css'
                );
                wp_enqueue_style( 'crb-my-shiny-gutenberg-block-stylesheet' );
            }*/

            Block::make( __( 'Layout item' ) )
                ->set_category( 'guu_blocks' )
                ->set_preview_mode( true )
                ->set_inner_blocks( true )
                //->set_editor_style( 'crb-my-shiny-gutenberg-block-stylesheet' )
                ->set_render_callback( function ( $fields, $attributes, $inner_blocks ) {?>
                    <div class="layout__row">
                        <div class="full-content">
                            <?php echo $inner_blocks; ?>
                        </div>
                    </div>
                <?php } );
        }

        public function blockquoteWithAuthor()
        {
            Block::make( 'guu-blockquote', __( 'Цитата' ) )
                ->add_fields(array(
                    Field::make('complex', 'blockquote_loop', 'Цитата')
                        ->set_collapsed(true)
                        ->add_fields( array(
                            Field::make( 'rich_text', 'guu_blockquote', __( 'Цитата' ) )->set_required(),
                            Field::make('html', 'html1')->set_html('<h6 style="background: lightsteelblue; text-align: center;"><strong>Автор</strong></h6>'),
                            Field::make( 'select', 'guu_blockquote_show_author', 'Добавить автора' )
                                ->add_options( array(
                                    'no' => 'Нет',
                                    'yes' => 'Да',
                                ) ),
                            Field::make( 'text', 'guu_blockquote_author_name', __( 'Имя автора' ) )
                                ->set_conditional_logic( array(
                                    'relation' => 'AND',
                                    array(
                                        'field' => 'guu_blockquote_show_author',
                                        'value' => 'yes',
                                        'compare' => '=',
                                    )
                                )),
                            Field::make( 'image', 'guu_blockquote_author_image', __( 'Фото' ) )
                                ->set_conditional_logic( array(
                                    'relation' => 'AND',
                                    array(
                                        'field' => 'guu_blockquote_show_author',
                                        'value' => 'yes',
                                        'compare' => '=',
                                    )
                                )),
                            Field::make( 'rich_text', 'guu_blockquote_author_description', __( 'Описание' ) )
                                ->set_conditional_logic( array(
                                    'relation' => 'AND',
                                    array(
                                        'field' => 'guu_blockquote_show_author',
                                        'value' => 'yes',
                                        'compare' => '=',
                                    )
                                )),
                            Field::make( 'text', 'guu_blockquote_author_link', __( 'Ссылка' ) )
                                ->set_conditional_logic( array(
                                    'relation' => 'AND',
                                    array(
                                        'field' => 'guu_blockquote_show_author',
                                        'value' => 'yes',
                                        'compare' => '=',
                                    )
                                ))
                        ))
                        ->set_header_template('
                        <% if (guu_blockquote) { %>
                            Цитата 
                        <% } %>
                    ')
                ))
                ->set_icon( 'format-quote' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields ) {
                    if(!empty($fields['blockquote_loop'])){
                        foreach($fields['blockquote_loop'] as $blockquote){
                            ?>
                            <blockquote><?php echo apply_filters( 'the_content', $blockquote['guu_blockquote'] ); ?></blockquote>
                            <?php if($blockquote['guu_blockquote_show_author'] == 'yes'){?>
                                <a href="<?php if(empty($blockquote['guu_blockquote_author_link'])){?>javascript:void(0)<?php }else{ echo esc_html( $blockquote['guu_blockquote_author_link'] );}?>" class="user user--quote">
                                    <div class="user__img">
                                        <img src="<?php if(empty($blockquote['guu_blockquote_author_image'])){ echo get_template_directory_uri()."/assets/app/img/user-ava.png"; }else{ echo wp_get_attachment_image_url( $blockquote['guu_blockquote_author_image']);}?>" alt=""
                                             class="object-fit is--cover" data-object-fit="cover">
                                    </div>
                                    <div class="user__info">
                                        <div class="user__info-text">
                                            <?php echo apply_filters( 'the_content', $blockquote['guu_blockquote_author_description'] ); ?>
                                        </div>
                                        <div class="user__info-name">
                                            <p><?php echo esc_html( $blockquote['guu_blockquote_author_name'] ); ?></p>
                                        </div>
                                    </div>
                                </a>
                            <?php }
                        }
                    }
                } );
        }

        public function dotsList()
        {
            Block::make('dots-list', __( 'Список Guu' ) )
                ->add_fields(array(
                    Field::make('complex', 'dots', 'Елемент списка')
                        ->set_collapsed(true)
                        ->add_fields( array(
                            Field::make( 'text', 'list_item', __( 'Текст' ) ),
                        ) )
                        ->set_header_template('
                            <% if (list_item) { %>
                                <%- list_item %>
                            <% } else { %>
                                Новая персона
                            <% } %>
                        '),
                ))
                ->set_icon( 'editor-ul' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields, $attributes, $inner_blocks ) {
                    if(!empty($fields['dots'])){?>
                        <ol class="point-list">
                            <?php foreach($fields['dots'] as $dot){?>
                                <li class="point__item"><?php echo $dot['list_item'];?></li>
                            <?php } ?>
                        </ol>
                        <?php
                    }
                } );
        }

        public function sliderGallery()
        {
            Block::make('slider-guu', __( 'Слайдер' ) )
                ->add_fields(array(
                    Field::make('html', 'html1')->set_html('<h6 style="background: #f75a5a; "><span class="dashicons dashicons-info" style="color: #fff;"></span><strong style="color: #fff;"> В имени фото не должно быть Кирилицы</strong></h6>'),
                    Field::make( 'media_gallery', 'guu_media_gallery', __( 'Media Gallery' ) )
                ))
                ->set_preview_mode( true )
                ->set_icon( 'images-alt2' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields ) {
                    if(!empty($fields['guu_media_gallery'])){
                        $gallery_hash_id = md5(serialize($fields));?>
                        <div class="sliders">
                            <div class="gallery-slider">
                                <?php foreach($fields['guu_media_gallery'] as $gallery_item_id){?>
                                    <div class="gallery__slide">
                                        <a href="<?php echo wp_get_attachment_image_src( $gallery_item_id , "full")[0];?>" class="fancybox"
                                           data-fancybox="<?php echo $gallery_hash_id;?>">
                                            <?php echo wp_get_attachment_image( $gallery_item_id, "full", "", array( "class" => "object-fit is--cover", "data-object-fit" => "cover" ) );  ?>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="gallery-slider-nav">
                                <?php foreach($fields['guu_media_gallery'] as $gallery_item_id){?>
                                    <div class="gallery__slide">
                                        <?php echo wp_get_attachment_image( $gallery_item_id, "thumbnail", "", array( "class" => "object-fit is--cover", "data-object-fit" => "cover" ) );  ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                    }
                } );
        }

        public function cellGallery()
        {
            Block::make('cell-gallery-guu', __( 'Галерея табличная' ) )
                ->add_fields(array(
                    Field::make('html', 'html1')->set_html('<h6 style="background: #f75a5a; "><span class="dashicons dashicons-info" style="color: #fff;"></span><strong style="color: #fff;"> В имени фото не должно быть Кирилицы</strong></h6>'),
                    Field::make( 'media_gallery', 'guu_media_gallery', __( 'Media Gallery' ) )
                ))
                ->set_preview_mode( true )
                ->set_icon( 'screenoptions' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields ) {
                    if(!empty($fields['guu_media_gallery'])){
                        $gallery_hash_id = md5(serialize($fields)); ?>
                        <div class="fancybox-wrap">
                            <div class="fancybox-list">
                                <?php foreach($fields['guu_media_gallery'] as $gallery_item_id){?>
                                    <div class="fancybox__item">
                                        <a href="<?php echo wp_get_attachment_image_src( $gallery_item_id , "full")[0];?>" class="fancybox fancybox--gallery" data-fancybox="<?php echo $gallery_hash_id;?>">
                                            <?php echo wp_get_attachment_image( $gallery_item_id, "thumbnail", "", array( "class" => "object-fit is--cover", "data-object-fit" => "cover" ) );  ?>
                                        </a>
                                    </div>
                                <?php }?>
                            </div>
                        </div>
                    <?php }
                } );
        }

        public function sliderSorting()
        {
            Block::make('slider-guu-sorting', __( 'Слайдер сортируемый' ) )
                ->add_fields(array(
                    Field::make('complex', 'slides', 'Слайды')
                        ->set_collapsed(true)
                        ->add_fields( array(
                            Field::make( 'image', 'img_id', __( 'Фото' ) ),
                        ) )
                        ->set_header_template('
                            <% if (img_id) { %>
                                Слайд id: <%- img_id %>
                            <% } else { %>
                                Новый слайд
                                <% } %>
                        '),
                ))
                ->set_preview_mode( true )
                ->set_icon( 'images-alt' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields ) {
                    if(!empty($fields['slides'])){
                        $gallery_hash_id = md5(serialize($fields)); ?>
                        <div class="sliders">
                            <div class="gallery-slider">
                                <?php foreach($fields['slides'] as $gallery_item_id){?>
                                    <div class="gallery__slide">
                                        <a href="<?php echo wp_get_attachment_image_src( $gallery_item_id['img_id'], "full" )[0];?>" class="fancybox"
                                           data-fancybox="<?php echo $gallery_hash_id;?>">
                                            <?php echo wp_get_attachment_image( $gallery_item_id['img_id'], "full", "", array( "class" => "object-fit is--cover", "data-object-fit" => "cover" ) );  ?>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="gallery-slider-nav">
                                <?php foreach($fields['slides'] as $gallery_item_id){?>
                                    <div class="gallery__slide">
                                        <?php echo wp_get_attachment_image( $gallery_item_id['img_id'], "thumbnail", "", array( "class" => "object-fit is--cover", "data-object-fit" => "cover" ) );  ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                    }
                } );
        }

        public function filesUpload()
        {
            Block::make('files-guu', __( 'Документ' ) )
                ->add_fields(array(
                    Field::make('complex', 'files', 'Файлы')
                        ->set_collapsed(true)
                        ->add_fields( array(
                            Field::make( 'text', 'file_name', __( 'Название' ) ),
                            Field::make( 'select', 'files_upload_type', 'Вариант загрузки файла' )
                                ->set_default_value( 'upload' )
                                ->add_options( array(
                                    'upload' => 'Загрузка файла',
                                    'url' => 'Ссылка',
                                ) ),
                            Field::make( 'file', 'id', __( 'Документ' ) )->set_type( array(
                                    'application/msword',
                                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                                    'application/excel',
                                    'application/vnd.ms-excel',
                                    'application/x-excel',
                                    'application/x-msexcel',
                                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                    'application/pdf')

                            )
                                ->set_conditional_logic( array(
                                    'relation' => 'AND',
                                    array(
                                        'field' => 'files_upload_type',
                                        'value' => 'upload',
                                        'compare' => '=',
                                    )
                                )),
                            Field::make( 'text', 'file_url', __( 'URL' ) )
                                ->set_conditional_logic( array(
                                    'relation' => 'AND',
                                    array(
                                        'field' => 'files_upload_type',
                                        'value' => 'url',
                                        'compare' => '=',
                                    )
                                )),
                            Field::make( 'select', 'files_mime_type', 'Формат Файла' )
                                ->set_default_value( 'pdf-icon' )
                                ->add_options( array(
                                    'pdf-icon' => 'PDF',
                                    'xls-icon' => 'XLS',
                                    'doc-icon' => 'DOC',
                                ) )
                                ->set_conditional_logic( array(
                                    'relation' => 'AND',
                                    array(
                                        'field' => 'files_upload_type',
                                        'value' => 'url',
                                        'compare' => '=',
                                    )
                                )),
                        ) )
                        ->set_header_template('
                            <% if (file_name) { %>
                                <%- file_name %>
                            <% } else { %>
                                Новый документ
                            <% } %>
                        '),
                ))
                ->set_preview_mode( true )
                ->set_icon( 'format-aside' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields ) {
                    if(!empty($fields['files'])){
                        ?>
                        <div class="documents">
                            <?php foreach($fields['files'] as $file){
                            if($file['files_upload_type'] == "upload"){
                            $mime_type =  wp_check_filetype( wp_get_attachment_url( $file['id'] ) )['ext'];
                            $mime_render = true;
                            if(strpos($mime_type, 'doc') !== false){
                                $mime_class = 'doc-icon';
                            }elseif(strpos($mime_type, 'xls') !== false){
                                $mime_class = 'xls-icon';
                            }elseif(strpos($mime_type, 'pdf') !== false){
                                $mime_class = 'pdf-icon';
                            }else{
                                $mime_render = false;
                            }
                            ?>
                            <a href="<?php echo wp_get_attachment_url( $file['id'] ); ?> " class="document" rel="noopener noreferrer">
                                <?php if($mime_render){?>
                                    <svg class="document__icon <?php echo $mime_class;?>">
                                        <use class="<?php echo $mime_class;?>__part"
                                             xlink:href="<?php echo get_template_directory_uri()?>/assets/app/img/icons/sprite.svg#<?php echo $mime_class;?>">
                                        </use>
                                    </svg>
                                <?php }?>
                                <?php }else{?>
                                <a href="<?php echo $file['file_url'];?>" class="document" rel="noopener noreferrer">
                                    <svg class="document__icon <?php echo $file['files_mime_type'];?>">
                                        <use class="<?php echo $file['files_mime_type'];?>__part"
                                             xlink:href="<?php echo get_template_directory_uri()?>/assets/app/img/icons/sprite.svg#<?php echo $file['files_mime_type'];?>">
                                        </use>
                                    </svg>
                                    <?php }?>
                                    <span class="document__text"><?php echo $file['file_name'];?></span>
                                </a>
                                <?php } ?>
                        </div>
                        <?php
                    }
                } );
        }

        public function acordeonBlock()
        {
            Block::make('acordeon-guu', __( 'Акордеон' ) )
                ->add_fields(array(
                    Field::make('complex', 'items', 'Слайды')
                        ->set_collapsed(true)
                        ->add_fields( array(
                            Field::make( 'text', 'item_title', __( 'Заголовок' ) ),
                            Field::make( 'rich_text', 'item_description', __( 'Текст' ) ),
                        ) )
                        ->set_header_template('
                            <% if (item_title) { %>
                                <%- item_title %>
                            <% } else { %>
                                Новый слайд
                                <% } %>
                        '),
                ))
                ->set_preview_mode( true )
                ->set_icon( 'list-view' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields ) {
                    if(!empty($fields['items'])){?>
                        <div class="toggle-list" data-toggle-wrap>
                            <?php foreach($fields['items'] as $key => $item){
                                $acordeon_hash_id = md5(serialize(array_merge ($item)));?>
                                <div class="toggle-list__item" >
                                    <div class="toggle-list__name" data-toggle="<?php echo $acordeon_hash_id;?>">
                                        <h5><?php echo (!empty($item['item_title'])) ? $item['item_title'] : "";?></h5>
                                    </div>
                                    <div class="toggle-list__content" data-toggle-show="<?php echo $acordeon_hash_id;?>">
                                        <?php echo apply_filters( 'the_content', $item['item_description'] ); ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <?php
                    }
                } );
        }

        public function scheduleBlock()
        {
            Block::make('schedule-guu', __( 'Расписание' ) )
                ->add_fields(array(
                    Field::make('complex', 'items', 'Слайды')
                        ->set_collapsed(true)
                        ->add_fields( array(
                            Field::make( 'text', 'lesson_first_time', __( 'Начало занятия' ) )->set_attribute( 'placeholder', '8.15' )->set_required(true)->set_width(25),
                            Field::make( 'text', 'lesson_last_time', __( 'Конец занятия' ) )->set_attribute( 'placeholder', '9.45' )->set_required(true)->set_width(25),
                            Field::make( 'text', 'lesson_desc', __( 'Описание' ) )->set_attribute( 'placeholder', '(1чю 30 мин.)' )->set_width(50),
                            Field::make( 'text', 'break_first_time', __( 'Начало перемены' ) )->set_attribute( 'placeholder', '9.45' )->set_required(true)->set_width(25),
                            Field::make( 'text', 'break_last_time', __( 'Конец перемены' ) )->set_attribute( 'placeholder', '9.55' )->set_required(true)->set_width(25),
                            Field::make( 'text', 'break_desc', __( 'Описание' ) )->set_attribute( 'placeholder', 'перемена' )->set_width(50),
                        ) )
                ))
                ->set_preview_mode( true )
                ->set_icon( 'list-view' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields ) {
                    if(!empty($fields['items'])){?>
                        <ol class="schedule">
                            <?php foreach($fields['items'] as $item){?>
                                <li class="schedule__block">
                                    <p class="schedule__info"><strong><?php echo $item['lesson_first_time'];?> – <?php echo $item['lesson_last_time'];?></strong> <?php if(!empty($item['lesson_desc'])){ echo $item['lesson_desc'];}?></p>
                                    <p class="schedule__info schedule__info--secondary"><strong><?php echo $item['break_first_time'];?> – <?php echo $item['break_last_time'];?> </strong> <?php if(!empty($item['break_desc'])){ echo $item['break_desc'];}?></p>
                                </li>
                            <?php } ?>
                        </ol>
                    <?php } ?>
                <?php } );
        }

        public function tagItem()
        {
            Block::make('tag-guu', __( 'block-label' ) )
                ->add_fields(array(
                    Field::make( 'text', 'tag_text', __( 'Label' ) ),
                ))
                ->set_category( 'guu_blocks' )
                ->set_icon( 'tagcloud' )
                ->set_preview_mode( true )
                ->set_render_callback( function ( $fields) {?>
                    <?php if(!empty($fields['tag_text'])){?>
                        <p class="text-label"><?php echo $fields['tag_text'];?></p>
                    <?php }?>
                <?php } );
        }

        public function socialItem()
        {
            $default_icon = THEME_URL.'/assets/app/img/socials/vk.svg';
            Block::make('social-guu', __( 'Социальные сети' ) )
                ->add_fields(array(
                    Field::make('complex', 'social', 'Социальные сети')
                        ->set_collapsed(true)
                        ->add_fields(array(
                            Field::make('select', 'type', 'Icon Type')
                                ->set_width(30)
                                ->set_default_value( $default_icon )
                                ->set_options( array(
                                    THEME_URL.'/assets/app/img/socials/vk.svg' => 'Vk',
                                    THEME_URL.'/assets/app/img/socials/facebook.svg' => 'Facebook',
                                    THEME_URL.'/assets/app/img/socials/youtube.svg' => 'YouTube',
                                    THEME_URL.'/assets/app/img/socials/twitter.svg' => 'Twitter',
                                    THEME_URL.'/assets/app/img/socials/instagram.svg' => 'Instagram',
                                ) ),
                            Field::make('text', 'href', 'Link')->set_width(70)
                        ))
                        ->set_header_template('
                                <% if (href) { %>
                                    <%- href %>
                                <% } else { %>
                                    Новая социальная сеть
                                <% } %>
                            '),
                ))
                ->set_icon( 'instagram' )
                ->set_category( 'guu_blocks' )
                ->set_preview_mode( true )
                ->set_render_callback( function ( $fields) {?>
                    <?php if(!empty($fields['social'])){?>
                        <div class="content-socials">
                            <?php foreach($fields['social'] as $social){?>
                                <a href="<?php echo $social['href'];?>" target="_blank" class="content-socials__link" rel="nofollow noopener noreferrer">
                                    <img src="<?php echo $social['type'];?>" alt="" width="30">
                                </a>
                            <?php }?>
                        </div>
                    <?php }?>
                <?php } );
        }

        public function universalAcordeonBlock()
        {
            Block::make('universal-acordeon-guu', __( 'Акордеон универсальный' ) )
                ->add_fields(array(
                    Field::make( 'text', 'item_title', __( 'Заголовок' ) ),
                ))
                ->set_preview_mode( true )
                ->set_inner_blocks( true )
                ->set_inner_blocks_position( 'below' )
                ->set_icon( 'list-view' )
                ->set_category( 'guu_blocks', __( 'GUU Blocks' ) )
                ->set_render_callback( function ( $fields, $attributes, $inner_blocks ) {
                    if(!empty($fields)){
                        $acordeon_hash_id = md5(serialize(array_merge ($fields)).$inner_blocks);?>
                        <div class="toggle-list" data-toggle-wrap>
                            <div class="toggle-list__item" >
                                <div class="toggle-list__name" data-toggle="<?php echo $acordeon_hash_id;?>">
                                    <h5><?php echo (!empty($fields['item_title'])) ? $fields['item_title'] : "";?></h5>
                                </div>
                                <div class="toggle-list__content" data-toggle-show="<?php echo $acordeon_hash_id;?>">
                                    <?php echo $inner_blocks; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                } );
        }
    }

}
ThemeCustomBlocks::instance();